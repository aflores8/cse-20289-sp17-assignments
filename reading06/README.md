Reading 06
==========

A. MapReduce is trying to solve the problem of processing large amounts of data to produce other data.

B. Describe the three phases of a typical MapReduce workflow.
1. The programmer specifies two functions and inputs a set of key/value pairs.
2. MapReduce processes the input key/value pair and produces a set of intermediate pairs.
3. MapReduce then combines all intermediate values for a particular key and produces a set of merged output values 
(usually just one).
