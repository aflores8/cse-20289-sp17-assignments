#!/usr/bin/env python2.7


import sys
import string


acceptedchar = set(string.ascii_lowercase + string.digits + '-')
linenum = 1
for line in sys.stdin:
    for word in line.strip().split():
        filterword = [letter for letter in string.lower(word) if letter in acceptedchar]
        print '{}\t{}'.format("".join(filterword), linenum)
    linenum = linenum + 1