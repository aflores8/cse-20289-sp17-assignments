/* node.c */

#include "node.h"

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

/**
 * Create node. 
 *
 * This allocates and configures a node that the user must later deallocate.
 * @param   string      String value.
 * @param   next        Reference to next node.
 * @return  Allocated node structure.
 */
struct node * node_create(char *string, struct node *next){
  /* new struct allocated */
  struct node *newnode = malloc(sizeof(struct node));
  /* check if allocation fails */
  if(!newnode){
    return NULL;
  }
  /* string copy and allocated (strdup allocates memory for the string) */
  char *s = strdup(string);
  /* sets values to new node */
  newnode->string = s;
  newnode->number = atoi(s);
  newnode->next = next;
  
  return newnode;
}

/**
 * Delete node.
 *
 * This deallocates the given node (recursively if specified).
 * @param   n           Node to deallocate.
 * @param   recursive   Whether or not to deallocate recursively.
 * @return  NULL pointer.
 */
struct node * node_delete(struct node *n, bool recursive){
  /* if statement for recursive */
  struct node *curr = n;
  if(recursive){
    while(curr){
      n = curr->next;
      free(curr->string);
      free(curr);
      curr = n;
    }
  }
  else{
    if(n){
      free(n->string);
      free(n);
    }
  }
  return NULL;
}

/**
 * Dump node to stream.
 * 
 * This dumps out the node structure (Node{string, number, next}) to the stream.
 * @param   n       Node structure.
 * @param   stream  File stream.
 **/
void node_dump(struct node *n, FILE *stream){
  fprintf(stream, "Node{%s, %d, %p}\n", n->string, n->number, n->next);
}

/**
 * Compare node structures by number
 *
 * This compares two node structures by their number values.
 * @param   a       First node structure.
 * @param   b       Second node structure.
 * @return  < 0 if a < b, 0 if a == 0, > 0 if a > b
 */
int node_compare_number(const void *a, const void *b){
  int anum = (*(struct node **)a)->number;
  int bnum = (*(struct node **)b)->number;
  if(anum < bnum){
    return -1;
  }
  else if(anum > bnum){
    return 1;
  }
  else if(anum == 0){
    return 0;
  }
  return 0;
}

/**
 * Compare node structures by string
 *
 * This compares two node structures by their string values.
 * @param   a       First node structure.
 * @param   b       Second node structure.
 * @return  < 0 if a < b, 0 if a == 0, > 0 if a > b
 */
int node_compare_string(const void *a, const void *b){
  return strcmp((*(struct node **)a)->string, (*(struct node **) b)->string);
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
