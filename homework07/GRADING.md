GRADING.md
==========

Activity 1
----------

Makefile
--------
looks good.

node.c
------
good 

list.c
------
good

README.md
---------
* `-.25` 3: Time Complexity: O(nlogn) Average, O(n^2) Worst, Space Complexity: O(n) Average and Worst

Activity 2
----------

lsort.c
--------
looks good!

test_lsort.c
------------
passes all tests

benchmark
---------
passes all tests!

README.md
---------
good


**Total: 14.75/15**