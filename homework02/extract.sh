#!/bin/sh
STATUS=0
if [ $# -gt 0 ]; then
  while [ $# -gt 0 ]; do 
    case $1 in
      *.tgz)
        tar -xzvf $1 
        ;;
      *.tar.gz) 
        tar -xzvf $1
        ;;
      *.tbz)
        tar -xjvf $1  
        ;;
      *.tar.bz2)
        tar -xjvf $1
        ;;
      *.txz)
        tar -xJvf $1
        ;;
      *.tar.xz)
        tar -xJvf $1
        ;;
      *.zip)
        unzip $1
        ;;
      *.jar)
        unzip $1
        ;;    
    esac
    STATUS=1
    shift
  done
else
  echo Usage: extract.sh archive1 archive2...
fi
exit $STATUS

