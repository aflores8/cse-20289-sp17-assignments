/* map.c: separate chaining hash table */

#include "map.h"

/**
 * Create map data structure.
 * @param   capacity        Number of buckets in the hash table.
 * @param   load_factor     Maximum load factor before resizing.
 * @return  Allocated Map structure.
 */
Map * map_create(size_t capacity, double load_factor){
  Map* newMap = malloc(sizeof(Map));
  if(!newMap){
    return NULL;
  }
  if(capacity == 0){
    newMap->capacity = DEFAULT_CAPACITY;
  }
  else{
    newMap->capacity = capacity;
  }
  if(load_factor <= 0){
    newMap->load_factor = DEFAULT_LOAD_FACTOR;
  }
  else{
    newMap->load_factor = load_factor;
  }
  newMap->size = 0;
  Entry* buckets = calloc(newMap->capacity, sizeof(Entry));
  newMap->buckets = buckets;
  return newMap;
}

/**
 * Delete map data structure.
 * @param   m               Map data structure.
 * @return  NULL.
 */
Map * map_delete(Map *m){
  if(m){
    if(m->buckets){
      for(int i = 0; i < m->capacity; i++){
        entry_delete(m->buckets[i].next, true);
      }
      free(m->buckets);
    }
    free(m);
  }
  return NULL;
}

/**
 * Insert or update entry into map data structure.
 * @param   m               Map data structure.
 * @param   key             Entry's key.
 * @param   value           Entry's value.
 * @param   type            Entry's value's type.
 */
void map_insert(Map *m, const char *key, const Value value, Type type){
  /* checks to see if the new ratio is going to be lower than the load ratio, otherwise it resizes map
  need to typecast one size_t as double */
  if((double)m->size / m->capacity >= m->load_factor){
    map_resize(m, m->capacity * 2);
  }
  uint64_t bucket = fnv_hash(key, strlen(key)) % m->capacity;
  Entry* curr = &(m->buckets[bucket]);
  while(curr->next){
    curr = curr->next;
    if(strcmp(curr->key, key) == 0){
      entry_update(curr, value, type);
      return;
    }
  }
  curr->next = entry_create(key, value, NULL, type);
  m->size++;
}

/**
 * Search map data structure by key.
 * @param   m               Map data structure.
 * @param   key             Key of the entry to search for.
 * @param   Pointer to the entry with the specified key (or NULL if not found).
 */
Entry * map_search(Map *m, const char *key){
  uint64_t bucket = fnv_hash(key, strlen(key)) % m->capacity;
  Entry* entryPtr = m->buckets[bucket].next;
  while(entryPtr){
    if(strcmp(entryPtr->key, key) == 0){
      return entryPtr;
    }
    entryPtr = entryPtr->next;
  }
  return NULL;
}

/**
 * Remove entry from map data structure with specified key.
 * @param   m               Map data structure.
 * @param   key             Key of the entry to remove.
 * return   Whether or not the removal was successful.
 */
bool map_remove(Map *m, const char *key){
  Entry* found = map_search(m, key);
  uint64_t bucket = fnv_hash(key, strlen(key)) % m->capacity;
  if(!found){
    return false;
  }
  else{
    Entry* prev = &m->buckets[bucket];
    while(prev->next != found){
      prev = prev->next;
    }
    Entry* next = found->next;
    entry_delete(found, false);
    prev->next = next;
    m->size--;
    return true;
  }
}

/**
 * Dump all the entries in the map data structure.
 * @param   m               Map data structure.
 * @param   stream          File stream to write to.
 * @param   mode            Dump mode to use.
 */
void map_dump(Map *m, FILE *stream, const DumpMode mode){
  for(size_t bucket = 0; bucket < m->capacity; bucket++){
    Entry* curr = m->buckets[bucket].next;
    while(curr){
      entry_dump(curr, stream, mode);
      curr = curr->next;
    }
  }
}

/**
 * Resize the map data structure.
 * @param   m               Map data structure.
 * @param   new_capacity    New capacity for the map data structure.
 */
void map_resize(Map *m, size_t new_capacity){
  Entry* newBuckets = calloc(new_capacity, sizeof(Entry));
  for(size_t bucket = 0; bucket < m->capacity; bucket++){
    Entry* curr = m->buckets[bucket].next;
    Entry* temp = NULL;
    while(curr){
      uint64_t index = fnv_hash(curr->key, strlen(curr->key)) % new_capacity;
      temp = curr->next;
      curr->next = newBuckets[index].next;
      newBuckets[index].next = curr;
      curr = temp;
    }
  }
  m->capacity = new_capacity;
  free(m->buckets);
  m->buckets = newBuckets;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
