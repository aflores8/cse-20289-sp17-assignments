| NITEMS | ALPHA  |    TIME    |   SPACE   |
|--------|--------|------------|-----------|
|1       | 0.5    | 0.00       | 0.58      |
|1       | 0.75   | 0.00       | 0.58      |
|1       | 0.9    | 0.00       | 0.58      |
|1       | 1.0    | 0.00       | 0.58      |
|1       | 2.0    | 0.00       | 0.58      |
|1       | 4.0    | 0.00       | 0.58      |
|1       | 8.0    | 0.00       | 0.58      |
|1       | 16.0   | 0.00       | 0.58      |
|10      | 0.5    | 0.00       | 0.58      |
|10      | 0.75   | 0.00       | 0.58      |
|10      | 0.9    | 0.00       | 0.58      |
|10      | 1.0    | 0.00       | 0.58      |
|10      | 2.0    | 0.00       | 0.58      |
|10      | 4.0    | 0.00       | 0.58      |
|10      | 8.0    | 0.00       | 0.58      |
|10      | 16.0   | 0.00       | 0.57      |
|100     | 0.5    | 0.00       | 0.59      |
|100     | 0.75   | 0.00       | 0.59      |
|100     | 0.9    | 0.00       | 0.58      |
|100     | 1.0    | 0.00       | 0.58      |
|100     | 2.0    | 0.00       | 0.59      |
|100     | 4.0    | 0.00       | 0.59      |
|100     | 8.0    | 0.00       | 0.58      |
|100     | 16.0   | 0.00       | 0.58      |
|1000    | 0.5    | 0.00       | 0.69      |
|1000    | 0.75   | 0.00       | 0.71      |
|1000    | 0.9    | 0.00       | 0.71      |
|1000    | 1.0    | 0.00       | 0.66      |
|1000    | 2.0    | 0.00       | 0.66      |
|1000    | 4.0    | 0.00       | 0.66      |
|1000    | 8.0    | 0.00       | 0.66      |
|1000    | 16.0   | 0.00       | 0.66      |
|10000   | 0.5    | 0.01       | 2.63      |
|10000   | 0.75   | 0.01       | 1.82      |
|10000   | 0.9    | 0.01       | 1.82      |
|10000   | 1.0    | 0.01       | 1.89      |
|10000   | 2.0    | 0.01       | 1.56      |
|10000   | 4.0    | 0.01       | 1.44      |
|10000   | 8.0    | 0.01       | 1.37      |
|10000   | 16.0   | 0.01       | 1.35      |
|100000  | 0.5    | 0.15       | 17.51     |
|100000  | 0.75   | 0.17       | 20.01     |
|100000  | 0.9    | 0.14       | 12.17     |
|100000  | 1.0    | 0.15       | 12.18     |
|100000  | 2.0    | 0.14       | 10.19     |
|100000  | 4.0    | 0.14       | 9.19      |
|100000  | 8.0    | 0.16       | 8.68      |
|100000  | 16.0   | 0.20       | 8.44      |
|1000000 | 0.5    | 1.69       | 140.84    |
|1000000 | 0.75   | 1.87       | 156.50    |
|1000000 | 0.9    | 1.99       | 168.51    |
|1000000 | 1.0    | 1.69       | 108.85    |
|1000000 | 2.0    | 1.78       | 92.85     |
|1000000 | 4.0    | 2.01       | 84.85     |
|1000000 | 8.0    | 2.52       | 80.85     |
|1000000 | 16.0   | 4.04       | 78.85     |
