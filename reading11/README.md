Reading 11
==========

1. Creating a file descriptor or endpoint for communicating over the network.
socket_fd = socket(p->ai_family, p->ai_socktype, p->ai_protocol);

2. Lookup the address information for a particular machine or service.
getaddrinfo("www.bbc.com", NULL, &hints, &infoptr)

3. Assign an address to a server network file descriptor (aka assign a name to a socket).
bind(fileDesc, p->ai_addr, p->ai_addrlen)

4. Convert the network address structure into corresponding host and service strings.
getnameinfo(&caddr, clen, host, sizeof(host), service, sizeof(service), flags)

5. Establish a client network file descriptor to a specified address.
client_fd = accept(server_fd, &client_addr, &client_len)

6. Mark network file descriptor as waiting for incoming connections.
listen(socket_fd, SOMAXCONN)

7. Create a new network file descriptor based on a incoming connection.
client_fd = accept(server_fd, &client_addr, &client_len)

