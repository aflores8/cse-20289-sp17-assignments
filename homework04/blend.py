#!/usr/bin/env python2.7

import atexit
import os
os.environ['PATH'] = '~ccl/software/external/imagemagick/bin:' + os.environ['PATH']
import re
import shutil
import sys
import tempfile

import requests

# Global variables

REVERSE     = False
DELAY       = 20
STEPSIZE    = 5
index       = 0
# Functions

def usage(status=0):
    print '''Usage: {} [ -r -d DELAY -s STEPSIZE ] netid1 netid2 target
    -r          Blend forward and backward
    -d DELAY    GIF delay between frames (default: {})
    -s STEPSIZE Blending percentage increment (default: {})'''.format(
        os.path.basename(sys.argv[0]), DELAY, STEPSIZE
    )
    sys.exit(status)

def cleanup():
	shutil.rmtree(workspace)
	print  'Cleaning Up Workspace: {}'.format(workspace)
atexit.register(cleanup)
	
def search_portrait(netid):
	url = 'https://cse.nd.edu/people/profiles/{}'.format(netid)
	response = requests.get(url)
	if response.status_code != 200:
		print "Error getting URL"
		sys.exit(1)
	RX = "https:\/\/engineering.nd.edu\/profiles\/" + netid + "\/@@images\/[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}\.jpeg"
	purl = re.findall(RX, response.text)
	print 'Searching portrait for {}... {}'.format(netid, purl[0])
	if len(purl[0] < 1):
		print 'URL not found'
		sys.exit(1)
	return purl[0]

def download_file(url, path):
	print 'Downloading {} to {}'.format(url, path)
	r = requests.get(url)
	if r.status_code != 200:
		print "Error"
		sys.exit(1)
	with open(path, 'wb') as fs:
	    fs.write(r.content)

def run_command(command):
	os.system(command)

# Parse command line options

args = sys.argv[1:]

while len(args) and args[0].startswith('-') and len(args[0]) > 1:
    arg = args.pop(0)
    if arg == '-r':
        REVERSE = True
    elif arg == '-d':
        DELAY = args.pop(0)
    elif arg == '-s':
        STEPSIZE = args.pop(0)
    elif arg == '-h':
        usage(0)
    else:
        usage(1)

if len(args) != 3:
    usage(1)

netid1 = args[0]
netid2 = args[1]
target = args[2]

# Main execution

# Create workspace
workspace = tempfile.mkdtemp(prefix = 'blend')
print 'Using workspace: {}'.format(workspace)

# Extract portrait URLs
purl1 = search_portrait(netid1)
purl2 = search_portrait(netid2)

# Download portraits
path1 = os.path.join(workspace, netid1 + '.jpeg')
path2 = os.path.join(workspace, netid2 + '.jpeg')
download_file(purl1, path1)
download_file(purl2, path2)

compos = []

# Generate blended composite images
if(REVERSE == False):
	while index <= 100:
		nstr = str(index)
		mixname = workspace + nstr + '-' + target
		fp1 = workspace + '/' + netid1 + '.jpeg'
		fp2 = workspace + '/' + netid2 + '.jpeg'
		run_command('composite -blend {} {} {} {}'.format(index, fp1, fp2, mixname))
		compos.append(mixname)
		index += STEPSIZE
		print '{} ... SUCCESS!'.format(mixname)
else:
	while index <= 100:
		nstr = str(index)
		mixname = workspace + nstr + '-' + target
		fp1 = workspace + '/' + netid1 + '.jpeg'
		fp2 = workspace + '/' + netid2 + '.jpeg'
		run_command('composite -blend {} {} {} {}'.format(index, fp2, fp1, mixname))
		compos.append(mixname)
		index += STEPSIZE
		print '{} ... SUCCESS!'.format(mixname)
	

# Generate final animation

string_command = 'convert -delay {} {} {}'.format(DELAY, ' '.join(compos), target)
run_command(string_command)