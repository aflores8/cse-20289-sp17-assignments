#!/bin/sh
#broify.sh
#Name: Alan Flores

DELIM="#"
W=""

usage(){
  cat <<EOF
Usage: $(basename $0)
  -d DELIM    Use this as the comment delimiter.
  -W          Don't strip empty lines.
EOF
      exit $1
}

while [ $# -gt 0 ]; do
  case $1 in
  -d) DELIM=$2 shift ;;
  -W) W="-W" ;;
  -h) usage 0 ;;
  *) usage 1 ;;
  esac
  shift
done

DELIM=`echo $DELIM | cut -c 1`
if [ -z $W ]; then
  cut -d "$DELIM" -f1 | sed "s|[[:space:]]*$||g" | sed '/^$/d' #deletes whitespace and comments
else
  cut -d "$DELIM" -f1 | sed "s|[[:space:]]*$||g" #deletes comments but not whitespace
fi
