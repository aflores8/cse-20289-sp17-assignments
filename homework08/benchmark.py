#!/usr/bin/env python2.7

import os
import re

i = 0
n = 1
time1 = []
time2 = []
time3 = []
time4 = []
time5 = []
time6 = []
time7 = []
time8 = []
space1 = []
space2 = []
space3 = []
space4 = []
space5 = []
space6 = []
space7 = []
space8 = []
string = ''
stringt = ''
while n < 10000000:
	string = os.popen('shuf -i1-100000000 -n ' + str(n) + ' | ./measure ./freq -l 0.5 2>&1 > /dev/null').read().rstrip()
	stringt = re.search('([0-9]*\.[0-9]{2})[0-9]{4} seconds\t([0-9]*\.[0-9]{2})[0-9]{4} Mbytes', string)
	if stringt:
		time1.append(stringt.group(1))
		space1.append(stringt.group(2))
	n = n * 10
n = 1
while n < 10000000:
	string = os.popen('shuf -i1-100000000 -n ' + str(n) + ' | ./measure ./freq -l 0.75 2>&1 > /dev/null').read().rstrip()
	stringt = re.search('([0-9]*\.[0-9]{2})[0-9]{4} seconds\t([0-9]*\.[0-9]{2})[0-9]{4} Mbytes', string)
	if stringt:
		time2.append(stringt.group(1))
		space2.append(stringt.group(2))
	n = n * 10
n = 1
while n < 10000000:
	string = os.popen('shuf -i1-100000000 -n ' + str(n) + ' | ./measure ./freq -l 0.9 2>&1 > /dev/null').read().rstrip()
	stringt = re.search('([0-9]*\.[0-9]{2})[0-9]{4} seconds\t([0-9]*\.[0-9]{2})[0-9]{4} Mbytes', string)
	if stringt:
		time3.append(stringt.group(1))
		space3.append(stringt.group(2))
	n = n * 10
n = 1
while n < 10000000:
	string = os.popen('shuf -i1-100000000 -n ' + str(n) + ' | ./measure ./freq -l 1.0 2>&1 > /dev/null').read().rstrip()
	stringt = re.search('([0-9]*\.[0-9]{2})[0-9]{4} seconds\t([0-9]*\.[0-9]{2})[0-9]{4} Mbytes', string)
	if stringt:
		time4.append(stringt.group(1))
		space4.append(stringt.group(2))
	n = n * 10
n = 1
while n < 10000000:
	string = os.popen('shuf -i1-100000000 -n ' + str(n) + ' | ./measure ./freq -l 2.0 2>&1 > /dev/null').read().rstrip()
	stringt = re.search('([0-9]*\.[0-9]{2})[0-9]{4} seconds\t([0-9]*\.[0-9]{2})[0-9]{4} Mbytes', string)
	if stringt:
		time5.append(stringt.group(1))
		space5.append(stringt.group(2))
	n = n * 10
n = 1
while n < 10000000:
	string = os.popen('shuf -i1-100000000 -n ' + str(n) + ' | ./measure ./freq -l 4.0 2>&1 > /dev/null').read().rstrip()
	stringt = re.search('([0-9]*\.[0-9]{2})[0-9]{4} seconds\t([0-9]*\.[0-9]{2})[0-9]{4} Mbytes', string)
	if stringt:
		time6.append(stringt.group(1))
		space6.append(stringt.group(2))
	n = n * 10
n = 1
while n < 10000000:
	string = os.popen('shuf -i1-100000000 -n ' + str(n) + ' | ./measure ./freq -l 8.0 2>&1 > /dev/null').read().rstrip()
	string = os.popen('shuf -i1-100000000 -n ' + str(n) + ' | ./measure ./freq -l 8.0 2>&1 > /dev/null').read().rstrip()
	stringt = re.search('([0-9]*\.[0-9]{2})[0-9]{4} seconds\t([0-9]*\.[0-9]{2})[0-9]{4} Mbytes', string)
	if stringt:
		time7.append(stringt.group(1))
		space7.append(stringt.group(2))
	n = n * 10
n = 1
while n < 10000000:
	string = os.popen('shuf -i1-100000000 -n ' + str(n) + ' | ./measure ./freq -l 16.0 2>&1 > /dev/null').read().rstrip()
	stringt = re.search('([0-9]*\.[0-9]{2})[0-9]{4} seconds\t([0-9]*\.[0-9]{2})[0-9]{4} Mbytes', string)
	if stringt:
		time8.append(stringt.group(1))
		space8.append(stringt.group(2))
	n = n * 10
n = 1
print '| NITEMS | ALPHA  |    TIME    |   SPACE   |'
print '|--------|--------|------------|-----------|'
while i < 7:
	if n < 100000:
		print '|' + str(n) + ' \t | 0.5    | ' + time1[i] + '       | ' + space1[i] + '\t   |'
		print '|' + str(n) + ' \t | 0.75   | ' + time2[i] + '       | ' + space2[i] + '\t   |'
		print '|' + str(n) + ' \t | 0.9    | ' + time3[i] + '       | ' + space3[i] + '\t   |'
		print '|' + str(n) + ' \t | 1.0    | ' + time4[i] + '       | ' + space4[i] + '\t   |'
		print '|' + str(n) + ' \t | 2.0    | ' + time5[i] + '       | ' + space5[i] + '\t   |'
		print '|' + str(n) + ' \t | 4.0    | ' + time6[i] + '       | ' + space6[i] + '\t   |'
		print '|' + str(n) + ' \t | 8.0    | ' + time7[i] + '       | ' + space7[i] + '\t   |'
		print '|' + str(n) + ' \t | 16.0   | ' + time8[i] + '       | ' + space8[i] + '\t   |'
	elif n < 1000000:
		print '|' + str(n) + '  | 0.5    | ' + time1[i] + '       | ' + space1[i] + '\t   |'
		print '|' + str(n) + '  | 0.75   | ' + time2[i] + '       | ' + space2[i] + '\t   |'
		print '|' + str(n) + '  | 0.9    | ' + time3[i] + '       | ' + space3[i] + '\t   |'
		print '|' + str(n) + '  | 1.0    | ' + time4[i] + '       | ' + space4[i] + '\t   |'
		print '|' + str(n) + '  | 2.0    | ' + time5[i] + '       | ' + space5[i] + '\t   |'
		print '|' + str(n) + '  | 4.0    | ' + time6[i] + '       | ' + space6[i] + '\t   |'
		print '|' + str(n) + '  | 8.0    | ' + time7[i] + '       | ' + space7[i] + '\t   |'
		print '|' + str(n) + '  | 16.0   | ' + time8[i] + '       | ' + space8[i] + '\t   |'
	else:
		print '|' + str(n) + ' | 0.5    | ' + time1[i] + '       | ' + space1[i] + '\t   |'
		print '|' + str(n) + ' | 0.75   | ' + time2[i] + '       | ' + space2[i] + '\t   |'
		print '|' + str(n) + ' | 0.9    | ' + time3[i] + '       | ' + space3[i] + '\t   |'
		print '|' + str(n) + ' | 1.0    | ' + time4[i] + '       | ' + space4[i] + '\t   |'
		print '|' + str(n) + ' | 2.0    | ' + time5[i] + '       | ' + space5[i] + '\t   |'
		print '|' + str(n) + ' | 4.0    | ' + time6[i] + '       | ' + space6[i] + '\t   |'
		print '|' + str(n) + ' | 8.0    | ' + time7[i] + '       | ' + space7[i] + '\t   |'
		print '|' + str(n) + ' | 16.0   | ' + time8[i] + '       | ' + space8[i] + '\t   |'
	i = i + 1
	n = n*10