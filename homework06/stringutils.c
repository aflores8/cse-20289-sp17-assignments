/* stringutils.c: String Utilities */

#include "stringutils.h"

#include <ctype.h>
#include <string.h>
#include <math.h>

/**
 * Convert all characters in string to lowercase.
 * @param   s       String to convert
 * @return          Pointer to beginning of modified string
 **/
char * string_lowercase(char *s){
  char *c = s;
  while(*c != '\0'){ /* advance c, return s */
    *c = tolower(*c);
    c++;
  }
  return s;
}

/**
 * Convert all characters in string to uppercase.
 * @param   s       String to convert
 * @return          Pointer to beginning of modified string
 **/
char * string_uppercase(char *s){
  char *c = s;
  while(*c != '\0'){ /* advance c, return s */
    *c = toupper(*c);
    c++;
  }
  return s;
}

/**
 * Returns whether or not the 's' string starts with given 't' string.
 * @param   s       String to search within
 * @param   t       String to check for
 * @return          Whether or not 's' starts with 't'
 **/
bool string_startswith(char *s, char *t){
  if(!s || !t){
    return false; /* checks if null */
  }
  size_t lent = strlen(t);
  size_t lens = strlen(s);
  if(lens < lent){
    return false;
  }
  else{
    return strncmp(t, s, lent) == 0;
  }
}

/**
 * Returns whether or not the 's' string ends with given 't' string.
 * @param   s       String to search within
 * @param   t       String to check for
 * @return          Whether or not 's' ends with 't'
 **/
bool string_endswith(char *s, char *t){
  if(!s || !t){
    return false;
  }
  size_t lent = strlen(t);
  size_t lens = strlen(s);
  if(lens < lent){
    return false;
  }
  return strncmp(t, s + lens - lent, lent) == 0; /* compares string only at the end of the string */
}

/**
 * Removes trailing newline (if present).
 * @param   s       String to modify
 * @return          Pointer to beginning of modified string
 **/
char *	string_chomp(char *s){
  size_t lens = strlen(s);
  char *c = s + lens - 1;
  
  if(*c == '\n'){
    *c = 0;
  }
  return s;
}

/**
 * Removes whitespace from front and back of string (if present).
 * @param   s       String to modify
 * @return          Pointer to beginning of modified string
 **/
char * string_strip(char *s){
  char *e;

  while(*s == ' '){
    s++;
  }
  if(*s == 0){
    return s;
  }
  e = s + strlen(s) - 1;
  while(e > s && *e == ' '){
    e--;
  }
  *(e+1) = 0; /* creates new end character */
  return s;
}

/**
 * Reverses a string given the provided from and to pointers.
 * @param   from    Beginning of string
 * @param   to      End of string
 * @return          Pointer to beginning of modified string
 **/
static char * string_reverse_range(char *from, char *to){
  char temp;
  char* s = from;
  while(to > s){
    temp = *to;
    *to = *s;
    *s = temp;
    to--;
    s++;
  }
  return from;
}

/**
 * Reverses a string.
 * @param   s       String to reverse
 * @return          Pointer to beginning of modified string
 **/
char * string_reverse(char *s){
  char* end = s + strlen(s) - 1;
  string_reverse_range(s, end);
  return s;
}

/**
 * Reverses all words in a string.
 * @param   s       String with words to reverse
 * @return          Pointer to beginning of modified string
 **/
char * string_reverse_words(char *s){
  char *c = NULL;
  char *temp = s;

  while(*temp != '\0'){ 
    if ((c == NULL) && (*temp != ' ')){ /* makes sure that the string start with non-space only*/
      c = temp;
    }
    if(c && ((*(temp + 1) == ' ') || (*(temp + 1) == '\0'))){
      string_reverse_range(c, temp);
      c = NULL;
    }
    temp++;
  }
  string_reverse_range(s, temp - 1);
  return s;
}

/**
 * Replaces all instances of 'from' in 's' with corresponding values in 'to'.
 * @param   s       String to translate
 * @param   from    String with letter to replace
 * @param   to      String with corresponding replacment values
 * @return          Pointer to beginning of modified string
 **/
char * string_translate(char *s, char *from, char *to){
  char *c = s;
  char *to_c = to;
  char *from_c = from;
  if(strlen(to) >= strlen(from)){
    for(c = s; *c; c++){
      for(from_c = from; *from_c; from_c++, to_c++){
        if(*c == *from_c){
          *c = *to_c;
        }      
      }
      to_c = to;
    }
  }
  return s;
}

/**
 * Converts given string into an integer.
 * @param   s       String to convert
 * @param   base    Integer base
 * @return          Converted integer value
 **/
int	string_to_integer(char *s, int base){
  string_uppercase(s);
  char *c = s + strlen(s) - 1;
  int i;
  int power = 0;
  int tot = 0;
  while(*c){
    if(isalpha(*c)){
      i = *c - '0' - 7;
      tot = tot + i*pow(base, power);
    }
    else if(isalnum(*c)){
      i = *c - '0';
      tot = tot + i*pow(base, power);
    }
    power++;
    c--;
  }
  return tot;
}
/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
