#!/bin/sh

usage(){
  cat 1>&2 <<EOF
Usage: $(basename $0) [-n N -D]

-n N   Returns N items (default is 5).
-D     Rank in descending order.

This script rans items from STDIN numerically, then outputs the first N items:
EOF
  exit $1
}

N=5
D=

while [ $# -gt 0 ]; do
  case $1 in
    -n) N=$2 shift ;;
    -D) D=descend ;;
    -h) usage 0 ;;
    *) usage 1 ;;
  esac
  shift
done

if [ -z $D ]; then
  sort -n | head -n $N
else
  sort -nr | head -n $N
fi

