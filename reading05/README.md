Reading 05
==========

1.1. Importing sys allows you to use the sys.arg function.
1.2. It takes in each of the arguments in the command line after the command itself and goes through them in sequence.
1.3. The trailing comma makes it so that the arguments are printed on one line rather than multiple.

2.1. This line checks whether or not there was a flag called for the program, flags start with the '-' symbol, thus it
checks for that and goes until there are no flagged arguments.
2.2. The first block appends a '-' symbol should there be nothing put in after the initial argument of the program itself.
The second blocks checks if there is only a '-' symbol and if so takes in input from the keyboard as to what the path 
should be.
2.3. The rstrip function removes leading and trailing whitespace from a string by default. It is necessary so that there
isn't extra whitespace being displayed in the concatenation.