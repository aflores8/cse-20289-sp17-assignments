Comments:

- 4.3: `du -h /etc/* 2> /dev/null | sort -r -h | head -n 5` -1

- 4.2.a: does not correctly get pid -0.25

- 5.3: only one discovered -0.25

Score: 13.5/15
