Homework 04 - Grading
=====================

**Score**: 10.5/15

Deductions
----------
-.25 blend.py: Script should check status of command to generate composite blended images and command to generate animation and exit if failure
-.5 blend.py: Script has some logic errors with variable names and types (check usage function and in count when doing index += STEPSIZE because STEPSIZE is a str but should be int)
-2 blend.py: Script does not generate gifs for the 4 test examples
-1.25 reddit.py: The 5 test cases fail. The script should not run usage function if len(args) != 1
-.25 reddit.py: Script has no default REGEX
-.25 reddit.py: Script does not handle command line arguments properly

Comments
--------
