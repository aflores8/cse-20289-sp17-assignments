Homework 04
===========

Activity 1:

1. The arguments were parsed by popping the first argument after the command should it be prefixed with a '-', then
putting it through an if statement to see what to do with it, otherwise it skips over that and goes to the part where
the program accesses the arguments to get the netid's and the target name.

2. The temporary workspace was created using tempfile.mkdtemp(prefix = 'blend'), which creates a temporary directory
with the prefix 'blend'.

3. The url was extracted by going to the url of the faculty member, then using regex on findall to find the URL for the
image. After that, the first member of the list created was taken out, which is the portrait URL.

4. The images were downloaded by opening a path file, which puts the content into a file.

5. The images were blended using the composite function in a while loop, which iteratively creates images. The images
are then put into a list.

6. The list of images is converted to the final animation using the convert function, which takes in the delay, the list,
and names the gif the target name.

7. For the get functions, should their status return something other than 200, the program exits out due to failure and
gives an error message.

Activity 2:

1. The arguments were parsed by popping the first argument after the command should it be prefixed with a '-', then
putting it through an if statement to see what to do with it, otherwise it skips over that and goes to the part where
the program accesses the arguments to get the regular expression.

2. The JSON data was first found by going through the different keys. With response.json(), it gets the JSON data for the
for the subreddit. In the for loop, it went through the variable child in data['data']['children'], which allowed the 
program to iterate through the various articles.

3. In the for loop, if the search function with the regex and the child['data'][FIELD] string returned with a true value
then the program printed the information for that article.