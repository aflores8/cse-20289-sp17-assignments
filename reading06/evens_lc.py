#!/usr/bin/env python2.7

import sys

print ' '.join(num for num in sys.stdin.read().split() if int(num) % 2 == 0)