#!/usr/bin/env python2.7

import requests
import sys
import os
import re

FIELD = 'title'
LIMIT = 10
SUBREDDIT = 'linux'
headers  = {'user-agent': 'reddit-{}'.format(os.environ['USER'])}

def usage(status=0):
    print '''Usage: reddit.py [ -f FIELD -s SUBREDDIT ] regex
    -f FIELD        Which field to search (default: {})
    -n LIMIT        Limit number of articles to report (default: {})
    -s SUBREDDIT    Which subreddit to search (default: {})'''.format(
        os.path.basename(sys.argv[0]), field, limit, subreddit
    )
    sys.exit(status)

args = sys.argv[1:]
while len(args) and args[0].startswith('-') and len(args[0]) > 1:
    arg = args.pop(0)
    if arg == '-f':
        FIELD = args.pop(0)
    elif arg == '-n':
        LIMIT = args.pop(0)
    elif arg == '-s':
        SUBREDDIT = args.pop(0)
    elif arg == '-h':
        usage(0)
    else:
        usage(1)
URL = 'https://www.reddit.com/r/' + SUBREDDIT + '/.json'
if len(args) != 1:
	usage(1)
regex = args[0]

response = requests.get(URL, headers=headers)
data = response.json()

count = 0
for child in data['data']['children']:
	#foundchild
	if re.search(regex, child['data'][FIELD]):
		title = child['data']['title']
		author = child['data']['author']
		link = child['data']['url']
		count += 1
	else:
		continue
	print '{}.  Title: {}'.format(count, title)
	print '    Author: {}'.format(author)
	print '    Link: {}'.format(link)
	if count >= LIMIT:
		break
	