/* sort.c */

#include <stdio.h>
#include <stdlib.h>

/* Constants */

#define MAX_NUMBERS (1<<10)

/* Functions */

size_t read_numbers(FILE *stream, int numbers[], size_t n){
  size_t i = 0;

  while (i < n && scanf("%d", &numbers[i]) != EOF){
    i++;
  }

  return i;
}

int sum_numbers(int numbers[], size_t n){
    int total = 0;
    for (size_t i = 0; i < n; i++){
        total += numbers[i];
    }

    return total;
}

/* Main Execution */

int main(int argc, char *argv[]){
  int numbers[MAX_NUMBERS];
  size_t nsize;

  nsize = read_numbers(stdin, numbers, MAX_NUMBERS);
  for(int i = 1; i < nsize; i++){
    int temp = numbers[i];
    int j = i;
    while(j > 0 && temp < numbers[j - 1]){
      numbers[j] = numbers[j - 1];
      j--;
    }
    numbers[j] = temp;
  }
  for(size_t i = 0; i < nsize; i++){
    printf("%d", numbers[i]);
    if(i < nsize - 1){
      printf(" ");
    }
  }
  printf("\n");

  return EXIT_SUCCESS;
}