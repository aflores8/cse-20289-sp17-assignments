Homework 07
===========
Activity 1: Singly-Linked List Library
1. The node_create function allocates the sizeof(struct node) and the string contained within it using strdup. When being
deallocated in node_delete, first the string and then the node are deallocated. When the recursive flag is
called, the program goes through each next pointer and deletes them until there aren't any remaining pointers.
The next pointer is saved by a temporary pointer until the node is deleted.

2. In list_create, the sizeof(struct list) is allocated for the list to be created. Whne deallocated, both the
internal nodes and then the list itself are deallocated. The function for node delete is called on the head with the 
recursive flag true so that all of the nodes are deleted first.

3. The list_qsort function works by creating an array using the linked list given, once created, the array is
passed through qsort with the appropriate function and size, the pointers are updated to make the sorted list, 
and then the array is deallocated. The time complexity for the list_qsort function is O(n^2) in the worst case
as it may have to go through nxn times. In its worst case, it also takes O(log(n)) space complexity.

4. The list_reverse function works by first setting the tail pointer to the head and then calling the reverse
function on the head and NULL pointer. The reverse function calls itself recursively until each node is
reversed. The time complexity for the function is O(n) as it has to go through the entire linked list once.
The space complexity for the function is also O(n) as it recursively calls the reverse function until the end
of the list.

5. Describe how your list_msort function works and analyze its time complexity and space complexity in terms 
of Big-O notation.
The list_msort function works by calling the msort function, which splits a linked list, and then merges the
split nodes back together in sorted order by comparing them with the compare function given. 
The time complexity for list_msort is always O(nlogn), while the space complexity is O(logn).

Activity 2:

1. As the number of items increases in a merge sort, it starts to take longer but takes less space than the
quick sort. Thus, quick sort is faster even with worse time complexity, but has more space complexity than merge
sort does.
These results are surprising considering my knowledge on time complexity as it should mean that quick sort is
slower the more items it has.


2. The expected case is not the case due to the hardware. Because quick sort is based around
an array, it can be put through the cache, which runs very quickly, so in this case quick sort would run faster
due to this. This shows that there are more things that can affect real world performance than just what the
predicted complexities would show.

| NITEMS | SORT     | TIME      | SPACE    |
| ------ | -------- |-----------|----------|
|1       | Merge    | 0.0       | 0.5      |
|1       | Quick    | 0.0       | 0.5      |
|10      | Merge    | 0.0       | 0.5      |
|10      | Quick    | 0.0       | 0.5      |
|100     | Merge    | 0.0       | 0.5      |
|100     | Quick    | 0.0       | 0.5      |
|1000    | Merge    | 0.0       | 0.5      |
|1000    | Quick    | 0.0       | 0.6      |
|10000   | Merge    | 0.0       | 1.1      |
|10000   | Quick    | 0.0       | 1.3      |
|100000  | Merge    | 0.1       | 6.6      |
|100000  | Quick    | 0.0       | 8.1      |
|1000000 | Merge    | 1.8       | 61.5     |
|1000000 | Quick    | 1.6       | 76.8     |
