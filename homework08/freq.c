/* freq.c */

#include "map.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Globals */

char * PROGRAM_NAME = NULL;
double load = 0;
DumpMode mode = VALUE_KEY;
char *am = NULL;
/* Functions */

void usage(int status){
  fprintf(stderr, "Usage: %s\n", PROGRAM_NAME);
  fprintf(stderr, "    -f FORMAT        Output format (KEY, KEY_VALUE, VALUE, VALUE_KEY)\n");
  fprintf(stderr, "    -l LOAD_FACTOR   Load factor for hash table\n");
  exit(status);
}

void freq_stream(FILE *stream, double load_factor, DumpMode mode){
  Map* m = map_create(0, load_factor);
  char buffer[BUFSIZ];
  while(fscanf(stream, "%s", buffer) != EOF){
    Entry* found = map_search(m, buffer);
    if(found){
      found->value.number++;
      map_insert(m, buffer, found->value, NUMBER);
    }
    else{
      Value temp;
      temp.number = 1;
      map_insert(m, buffer, temp, NUMBER);
    }
  }
  map_dump(m, stdout, mode);
  map_delete(m);
}

/* Main Execution */

int main(int argc, char *argv[]){
  /* Parse command line arguments */
  int argind = 1;
  PROGRAM_NAME = argv[0];
  while(argind < argc && strlen(argv[argind]) > 1 && argv[argind][0] == '-'){
    char *arg = argv[argind++];
    switch(arg[1]){
      case 'f':
        am = argv[argind++];
        if(strcmp(am, "VALUE") == 0){
          mode = VALUE;
        }
        else if(strcmp(am, "KEY") == 0){
          mode = KEY;
        }
        else if(strcmp(am, "KEY_VALUE") == 0){
          mode = KEY_VALUE;
        }
        else{
          mode = VALUE_KEY;
        }
        break;
      case 'l':
        load = atof(argv[argind++]);
        load = (double)load;
        break;
      case 'h':
        usage(0);
        break;
      default:
        usage(1);
        break;
    }
  }
  /* Compute frequencies of data from stdin */
  freq_stream(stdin, load, mode);
  return EXIT_SUCCESS;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
