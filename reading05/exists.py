#!/usr/bin/env python2.7

import sys
import glob

for arg in sys.argv[1:]:
    if arg == '*':
        for file in glob.glob('*'):
            if glob.glob(file):
                print('%s exists!' % file)
            else:
                print('%s does not exist!' % file)
    elif glob.glob(arg):
        print('%s exists!' % arg)
    else:
        print('%s does not exist!' % arg)
    