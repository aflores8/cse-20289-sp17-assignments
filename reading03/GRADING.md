reading03 - Grading
===================

**Score**: 3.4

Deductions
----------
-0.25 - 6. Your command only filters for lines that start with a 4 and end in a
7, and allows letters to be contained in between. You should use grep '4[0-9]*7'
-0.25 - 8. You should have used the diff command.

Comments
--------
For 5, you can condense that into one command like this:
    cat /etc/passwd | sed 's|/bin/[batc]sh|/usr/bin/python' | grep python
    you can also use the underscore like you did before
Good work!
