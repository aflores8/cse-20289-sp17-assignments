Homework 06
===========

Activity 1: String Utilities Library
1. The string_reverse_words function words by finding words, by taking in characters as a string until
it gets to a space or a null character, it then flips them so that the are backwards. Once it reaches
the end of the string the entire string is flipped so that the words are reversed. Its time complexity
is O(n) as it goes through one while loop, while the space complexity is O(1), as it doesn't change
the space taken up based on the size.
2. The string_translate uses two for loops which iterate through both the string and then the to and
from strings. If a character in the string is equal to the from pointer, it is changed to the value
of what the to pointer is pointing to. The time complexity is O(n^2) as it goes through two while loops
with a space complexity of O(1) as the space taken up isn't changed. It can be changed to O(n) time
complexity using a lookup table instead of two for loops.
3. The string_to_integer function works by iterating through a string, taking in the integer values of the
different characters based on what the base is of. The value is subtracted by '0', or '0' and 7 if it is
an alphabetical character. The value is raised to the power, which iterates to increase for each place of
the string. The value is then added to the total. The time complexity is O(n) as it only goes through one
loop, while the space complexity is only O(1).
4. A shared library is used when an executable is run and is inserted when the command is
executed. A static library is built into the program, and is already there when it it executed.
The libstringutils.a static library is smaller than the libstringutils.so shared library
as the shared library is an executable, which has its own overhead and requires more memory.

Activity 2: Translate
1. The program goes through the commmand line arguments by going through argv, if there is a value that
start with the '-' symbol, it goes to see which argument it is, that argument corresponds to a bitmask
that is added into the mode. If there's a source and a target, they are also grabbed.
The translation function takes in the stream, the source, the target, and the mode. If it finds that the
mode is and for one of the various enumerated values, then it runs that function after running chomp
and translate first.
2. The translate-static executable is much larger as it already has the library built into it,
so it requires more memory to contain it, while the translate-dynamic executable is only the executable.
The translate-static executable executes because it already has the library built into it.
The translate-dynamic executable doesn't execute unless you specify the path for the library is 
specified.