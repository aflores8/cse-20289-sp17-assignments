#!/bin/sh

if [ "$#" -eq "0" ]; then
  echo "No arguments given!"
else
  for i in "$@"
  do
    if [ -e "$i" ]; then
      echo "$i exists!"
    elif [ ! -e "$i" ]; then
      echo "$i does not exist!"
    else
      echo "Test failed"
    fi
  done
fi
