Reading 07
==========

Activity 1:
1. The read_numbers function takes in the standard input from the commmand line using the scanf function, which puts each of the decimal
values into the numbers array until the EOF signal is given (ctrl-d). While this is occurring, the size_t variable i is incrementing to
get the total size of the array. The value of i is then returned as the total size of the array inputted.
2. It does work to the same effect, as sizeof returns the size_t value for the size of what it is applied to. The sizeof returns the same value
that the read_numbers function does.

Activity 2:
1. The two programs are similar in the way that they parse through the command line arguments. Both use arg to take in thecommand line arguments. 
The argc variable is the number of the command line argument, while argv is the array of command line arguments. The grep.c takes requires more 
functions to perform the grep while the grep.py takes less as the functions are already built into Python.
2. While there are still arguments to go through, the cat program takes in the next argument as the path using char *path = argv[argind++],
which calls the next item in the argv array. However, if the argument is a -, then the reads in the standard input to put into the cat_stream 
function. Otherwise, it takes the file path and puts it through the cat_file function to open it.
3. The fgets function takes in the stream, and splits it up so that the fputs function can place them into the standard output.
The fgets function reads in at most one less than size characters from stream and stores them into the buffer pointed to by s.  Reading 
stops after an EOF or a newline.
4. The cat_file function works to open and read the file given by the file path, then returns that as a stream so that the cat_stream function
can read it. The if statement checks if the file doesn't actually exist, returning an error to the user if so with the standard error and the
number of the error using stderr and errno respectively.