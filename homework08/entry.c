/* entry.c: map entry */

#include "map.h"

/**
 * Create entry structure.
 * @param   key             Entry's key.
 * @param   value           Entry's value.
 * @param   next            Reference to next entry.
 * @param   type            Entry's value's type.
 * @return  Allocate Entry structure.
 */
Entry * entry_create(const char *key, const Value value, Entry *next, Type type){
  Entry *newentry = malloc(sizeof(Entry));
  /* check if allocation fails */
  if(!newentry){
    return NULL;
  }
  /* string copy and allocated (strdup allocates memory for the string) */
  char *k = strdup(key);
  /* sets values to new entry using entry_update */
  newentry->key = k;
  newentry->type = type;
  if(type == STRING){
    char *v = strdup(value.string);
    newentry->value.string = v;
  }
  else{
    newentry->value.number = value.number;
  }
  newentry->next = next;
  return newentry;
}

/**
 * Delete entry structure.
 * @param   e               Entry structure.
 * @param   recursive       Whether or not to delete remainder of entries.
 * return   NULL.
 */
Entry * entry_delete(Entry *e, bool recursive){
  if(e){
    if(recursive){
      Entry* next = e->next;
      Entry* temp;
      while(next){
        temp = next->next;
        entry_delete(next, false);
        next = temp;
      }
    }
    free(e->key);
    if(e->type == STRING){
      free(e->value.string);
    }
    free(e);
  }
  return NULL;
}

/**
 * Update entry's value.
 * @param   e               Entry structure.
 * @param   value           New value for entry.
 * @param   type            New value's type.
 */
void entry_update(Entry *e, const Value value, Type type){
  if(e->type == STRING){
    free(e->value.string);
  }
  e->type = type;
  if(type == STRING){
    char *v = strdup(value.string);
    e->value.string = v;
  }
  else{
    e->value.number = value.number;
  }
}

/**
 * Dump entry.
 * @param   e               Entry structure.
 * @param   stream          File stream to write to.
 * @param   mode            Dump mode to use.
 */
void entry_dump(Entry *e, FILE *stream, const DumpMode mode){
  switch(mode){
    case KEY:
      fprintf(stream, "%s\n", e->key);
      break;
    case KEY_VALUE:
      if(e->type == STRING){
        fprintf(stream, "%s\t%s\n", e->key, e->value.string);
      }
      else{
        fprintf(stream, "%s\t%ld\n", e->key, (long)e->value.number); /*casts number to print*/
      }
      break;
    case VALUE:
      if(e->type == STRING){
        fprintf(stream, "%s\n", e->value.string);
      }
      else{
        fprintf(stream, "%ld\n", (long)e->value.number); /*casts number to print*/
      }
      break;
    case VALUE_KEY:
      if(e->type == STRING){
        fprintf(stream, "%s\t%s\n", e->value.string ,e->key);
      }
      else{
        fprintf(stream, "%ld\t%s\n", (long)e->value.number, e->key); /*casts number to print*/
      }
      break;
  }
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
