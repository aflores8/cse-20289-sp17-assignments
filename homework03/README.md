Homework 03
===========

Activity 1: Caesar Cipher
1.The program checks to see if the command line argument $1 has changed, and if it hasn't then it sets the shift
to the default. If $1 is a number then it sets the shift to that value.
2. The first two source sets were created by setting one equal to the alphabet in uppercase and the other in lowercase.
3. The second set was created by cutting the alphabet sets at the point that was determined by the shift and saving them into
variables, then putting the two separate cuts back together, setting them equal to the second set, this was done for both 
uppercase and lowercase. The upper and lower case sets were then put together
4. The sets were then used for encryption using tr $UPPERSOURCE$LOWERSOURCE $TARGET.

Activity 2: Broification
1. The program uses while loop and a case structure to go through the arguments and changes variables based on the
arguments provided. The loop shifts the arguments when there is a flag so that it can get to all the arguments. The 
-d flag takes another argument afterwards, so it shifts again to keep $1 for the flags.
2. The comments were removed using the cut command. Any time the delimiter for comments was found, everything after
that point was cut out.
3. The whitespace was removed by using the sed to command to change all the whitespace to nothing.
4. Should an argument be put in for -d, then the program takes in the next value given as the delimiter for comments.
Should the -W flag be put in then the value of W changes and  program goes to the else if statement for that value,
which causes the whitespace to be preserved.
Bash didn't like to keep my variables as anything but the defaults, so I had to switch to sh to get it to work.

Activity 3: Zip Codes
1. The program uses while loop and a case structure to go through the arguments and changes variables based on the
arguments provided. The loop shifts the arguments when there is a flag so that it can get to all the arguments. Certain
arguments such as -c, -f, and -s that take another argument afterwards shift again to keep $1 for the flags.
2. The zip codes were extracted by piping the curl command to grep, which looks for the city given, then it is piped 
through another grep, which looks for 5 numbers in a row, with no numbers around it. They are then sorted and piped through
uniq so there are no repeats.
3. The program filters by state by taking the state given through the flag and placing in the url, which goes through curl.
Spaces are first changed to %20 as that is what the url uses for spaces. I filtered by city by putting the city variable into
grep before getting the zip codes.
4. Text and csv were handled using an if and else if statement. Text is the default, while in csv, all newlines are replaced
with commas and then the last comma is replaced by a newline to make it look nicer.
Bash didn't like to keep my variables as anything but the defaults, so I had to switch to sh to get it to work.