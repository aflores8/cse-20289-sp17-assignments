#!/usr/bin/env python2.7

import sys

print ' '.join(map(str, filter(lambda num: num % 2 == 0, map(int, sys.stdin.read().split()))))