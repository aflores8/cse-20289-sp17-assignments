Reading 02
==========

1. /bin/sh exists.sh
2. You must set the executable and readable bit using chmod. The command chmod 755 exists.sh allows it to be executed.
3. ./exists.sh
4. It makes it so that everything in the file is interpreted by the Bourne shell.
5. It returns: "exists.sh exists!"
6. It is the first parameter after the basename. If a parameters are put into the command line with the basename, exists.sh,
then the first parameter put in after exists.sh takes the place of $1.
7. The test -e "$1" command checks to see if the given file name is the name of a file that exists.
8. This script takes in the value of the first parametergiven in the command line after exists.sh, and sees whether or not that 
file exists, informing the user whether or not the file exists through text