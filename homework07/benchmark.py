#!/usr/bin/env python2.7

import os
import re

i = 0
n = 1
time = []
timeq = []
space = []
spaceq = []
string = ''
stringt = ''
while n < 10000000:
	string = os.popen('shuf -i1-100000000 -n ' + str(n) + ' | ./measure ./lsort -n 2>&1 > /dev/null').read().rstrip()
	stringt = re.search('([0-9]*\.[0-9]{1})[0-9]{5} seconds\t([0-9]*\.[0-9]{1})[0-9]{5} Mbytes', string)
	if stringt:
		time.append(stringt.group(1))
		space.append(stringt.group(2))
	n = n * 10
	i = i + 1 
n = 1
i = 0
while n < 10000000:
	string = os.popen('shuf -i1-100000000 -n ' + str(n) + ' | ./measure ./lsort -n -q 2>&1 > /dev/null').read().rstrip()
	stringt = re.search('([0-9]*\.[0-9]{1})[0-9]{5} seconds\t([0-9]*\.[0-9]{1})[0-9]{5} Mbytes', string)
	if stringt:
		timeq.append(stringt.group(1))
		spaceq.append(stringt.group(2))
	n = n * 10
	i = i + 1

i = 0
n = 1
print '| NITEMS | SORT     | TIME      | SPACE    |'
print '| ------ | -------- |-----------|----------|'
while i < 7:
	if n < 100000:
		print '|' + str(n) + ' \t | Merge    | ' + time[i] + '\t| ' + space[i] + '\t   |'
		print '|' + str(n) + ' \t | Quick    | ' + timeq[i] + '\t| ' + spaceq[i] + '\t   |'
	elif n < 1000000:
		print '|' + str(n) + '  | Merge    | ' + time[i] + '\t| ' + space[i] + '\t   |'
		print '|' + str(n) + '  | Quick    | ' + timeq[i] + '\t| ' + spaceq[i] + '\t   |'
	else:
		print '|' + str(n) + ' | Merge    | ' + time[i] + '\t| ' + space[i] + '\t   |'
		print '|' + str(n) + ' | Quick    | ' + timeq[i] + '\t| ' + spaceq[i] + '\t   |'
	i = i + 1
	n = n*10