/* lsort.c */

#include "list.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Globals */

char * PROGRAM_NAME = NULL;

/* Functions */

void usage(int status) {
  fprintf(stderr, "Usage: %s\n", PROGRAM_NAME);
  fprintf(stderr, "  -n   Numerical sort\n");
  fprintf(stderr, "  -q   Quick sort\n");
  exit(status);
}

void lsort(FILE *stream, bool numeric, bool quicksort){
  struct list *l = list_create();
  char buffer[BUFSIZ];
  char *c = buffer;
  while(fgets(c, BUFSIZ, stream)){
    list_push_back(l, c);
  }
  if(numeric){
    if(quicksort){
      list_qsort(l, node_compare_number);
    }
    else{
      list_msort(l, node_compare_number);
    }
  }
  else{
    if(quicksort){
      list_qsort(l, node_compare_string);
    }
    else{
      list_msort(l, node_compare_string);
    }
  }
  struct node * curr = l->head;
  while(curr){
    printf("%s", curr->string);
    curr = curr->next;
  }
  l = list_delete(l);
}

/* Main Execution */

int main(int argc, char *argv[]){
  /* Parse command line arguments */
  int argind = 1;
  bool num = false;
  bool quick = false;
  while(argind < argc && strlen(argv[argind]) > 1 && argv[argind][0] == '-'){
    char *arg = argv[argind++];
    switch(arg[1]){
      case 'n':
        num = true;
        break;
      case 'q':
        quick = true;
        break;
      case 'h':
        usage(0);
        break;
      default:
        usage(1);
        break;
    }
  }
  /* Sort using list */
  lsort(stdin, num, quick);
  return EXIT_SUCCESS;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
