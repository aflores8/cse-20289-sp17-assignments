Reading 01
==========

1.a. It is a pipeline that takes the standard output of the first command and pipes it into the standard input of the second
command. It takes the redirect command and puts it into the input of the sort command.
1.b. Standard error is the same as file descriptor number 2, so one can redirect standard error to the dev/null file,
known as the "bit bucket," so that you don't see that output.
1.c. It takes in the output and redirects it to the output.txt, either creating the file or replacing the current file
with the output.
1.d. The flags display file sizes in human-readable format instead of computer-readable bytes.
1.e. They are not the same as it would take the standard error of the ouput.txt redirect rather than that of etc.

2.a. echo *2002 | cat > 2002
2.b. echo *12 | cat > December
2.c. cat *-{01..06}
2.d. cat *{2002..2006..2}-{1..11..2}
2.e. cat *{2002..2004}-{09..12}

3.a. Huxley and Tux are executable by the owner.
3.b. Huxley and Tux are readable by those in the users group.
3.c. None of the files are writeable by anyone.
3.d. chmod 755 Tux
3.e. chmod 600 Tux

4.a. press ctrl-z
4.b. fg %1
4.c. ctrl+d sends an EOF, signalling the end of input to the bc process
4.d. kill -TERM PID
4.e. kill [-u user] -TERM bc