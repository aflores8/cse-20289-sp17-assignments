#!/bin/sh

#zipcode.sh
#Name: Alan Flores

CITY=""
FORMAT="text"
STATE=Indiana

usage(){
  cat <<EOF
Usage: $(basename $0)
  -c      CITY    Which city to search
  -f      FORMAT  Which format (text, csv)
  -s      STATE   Which state to search (Indiana)

If no CITY is specified, then all the zip codes for the STATE are displayed.
EOF
      exit $1
}

while [ $# -gt 0 ]; do
  case $1 in
  -c) CITY=$2 shift ;;
  -s) STATE=$2 shift ;;
  -f) FORMAT=$2 shift ;;
  -h) usage 0 ;;
  *) usage 1 ;;
  esac
  shift
done
STATE=`echo $STATE | sed 's/ /%20/g'` #%20 is a space in the url
SITE=http://www.zipcodestogo.com/"$STATE"/

if [ $FORMAT = "text" ]; then
  curl -s $SITE | grep "/$CITY/" | grep -Eo '[^0-9][0-9]{5}[^0-9]' | grep -Eo '[0-9]{5}' | sort | uniq
elif [ $FORMAT = "csv" ]; then
  curl -s $SITE | grep "/$CITY/" | grep -Eo '[^0-9][0-9]{5}[^0-9]' | grep -Eo '[0-9]{5}' | sort | uniq | tr '\n' , | sed '$s/,$/\n/'
else
  usage 1
fi
