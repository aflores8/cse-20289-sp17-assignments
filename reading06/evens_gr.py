#!/usr/bin/env python2.7

import sys

def evens(stream):
    i = 0
    v = stream.read().split()
    while i < len(v):
        if  int(v[i]) % 2 == 0:
            yield str(v[i])
        i += 1

print ' '.join(evens(sys.stdin))