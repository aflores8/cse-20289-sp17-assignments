GRADING - Exam 01
=================

- Commands:         2
- Short Answers:
    - Files:        2.75
    - Processes:    3
    - Pipelines:    3
- Debugging:        2.75
- Code Evaluation:  3
- Filters:          2.75
- Scripting:        3
- Total:	    22.25
