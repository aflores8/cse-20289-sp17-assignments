#!/usr/bin/env python2.7

import os

# ps aux
x = os.popen('ps aux', 'r')

# awk '{print $1}'
lines = [str(line.split()[0]) for line in x.read().rstrip().split('\n')]
# sort | uniq | wc -l
result = dict()
for x in lines:
    if x not in result.keys():
        result.update({x:1})
    else:
        result[x] = result[x] + 1
              
print len(sorted(result))