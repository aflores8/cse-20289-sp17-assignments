Comments:

- you could do something like `*.tgz|*.tar.gz)` and `*.tar.bz2|*.tbz|*.tbz2)` in you case statement for different patterns that have the same command.

- 0.25: does not handle the case of unsupported archive formats.

- when you call cowsay in your script, you need to use `$cowsay`

Score: 14.75/15
