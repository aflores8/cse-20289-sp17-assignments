Reading 10
==========

1. The strerror(errno) call will give the error message when you call it.

2. The truncate(file, desiredlength) call will truncate a file to the desired length.

3. The lseek(filename, 10, SEEK_SET); call will get you shifted 10 bytes into a file

4. Create a struct node s, update its value using stat, then use S_ISDIR to see if it is a directory.
stat("/path", &s);
S_ISDIR(s.st_mode); 

5. Calling fork() will create a child process that is a copy of your current process.

6. Calling exec without a fork will replace your current code.

7. Calling kill(pid, SIGTERM) will terminate the process with the given pid.

8. WIFEXITED(status) or WEXITSTATUS(status) will both give the exit status of a child process.
