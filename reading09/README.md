Reading 09
==========

Activity 1:
1. The size of s is 16 bytes, while the size of u is 8 bytes.
2. A struct requires the sum of the size of all of its members in memory, while a union only requires
the same amount of memory as the largest member. While each member in a struct has its own memory block,
in a union all of the members share a single block of memory.

Activity 2:
1. The program outputs "fate is inexorable!"
2. The program works by taking in values into a union which is part of a struct. These values set
are bits that represent the value of chars, which are then converted into a string which is then printed
to make the message.
3. The bit dump shows that all data is stored in bits on the computer so that it can read the values given.
When values are cast, it is changing the bits so that it can be represented as a different data type.

Activity 3:
1. A collision is when two objects end up in the same bucket in a hash table.
2. With separate chaining, each bucket in a hash table is a structure that can hold all of the values with the
entries with the same index. When there is a collision the new object is added to the structure at that hash
3. With open addressing, when there's a collsion, the new colliding object is moved to the next open address
to be stored.

Activity 4:
1.
Bucket  Value
0   	
1	
2	    2, 72
3	    3, 13
4	    14
5	
6	    56
7	    7
8	    78, 68
9	    79

2.
Bucket	Value
0	    68
1	    14
2	    2
3	    3
4	    72
5	    13
6	    56
7	    7
8	    78
9	    79
