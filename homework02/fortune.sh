#!/bin/sh

cowsay=/afs/nd.edu/user15/pbui/pub/bin/cowsay
trap bye 1 2 15

bye()
{
  shuf -e -n 1 "Going so soon?" "Goodbye" "Return soon" | cowsay
  exit 1
}
 
shuf -e -n 1 "Hello $USER, ask a question" "Hello $USER, what question do you have for me today?" "Hi $USER, what can I do for you?" "Why $USER, what is on your mind?" | cowsay 
while [ "$QUESTION" = "" ]
do
  echo Question?
  read QUESTION 
done

shuf -e -n 1 "It is certain" "It is decidedly so" "Without a doubt" "Yes, definitely" "You may rely on it" "As I see it, yes" "Most likely" "Outlook good" "Yes" "Signs point to yes" "Reply hazy try again" "Ask again later" "Better not tell you now" "Cannot predict now" "Concentrate and ask again" "Don't count on it" "My reply is no" "My sources say no" "Outlook not so good" "Very doubtful" | cowsay

