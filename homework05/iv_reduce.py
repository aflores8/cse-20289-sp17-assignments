#!/usr/bin/env python2.7

import sys
import collections

counts = {}
for line in sys.stdin:
    k, v  = line.split('\t', 1)
    counts[k] = str(counts.get(k,'')).rstrip() + ' ' + v
    
for k, v in sorted(counts.items()):
    print '{}\t{}'.format(k, v),