Homework 01
===========
Activity 1
1.a. All of the directories allow you and the administrators full permissions. The Private directory only gives permissions to
you and the administrators. The home directory allows the nd_campus and authuser lookup permissions, while the Public 
directory allows nd_campus and authuser lookup, read, and lock permissions.
b. The Private directory is private as only you and the administrators have permissions in that directory, preventing others from 
acccessing it. The Public directory is public as it gives nd_campus and authuser read, lookup, and lock permissions, letting 
others have a lot more access to the files in the directory.
2.a. The file was unable to be created as I don't have the permissions to do that.
b. While I have full permissions in the Unix permissions, I am unable to access the web directory due to the ACL's, so the
ACL's take precedence over the Unix permissions on AFS. 

Activity 2

| Command                             | Elapsed Time  |
|-------------------------------------|---------------|
| cp /usr/share/pixmaps/* images      | 0.037 seconds |
| mv images pixmaps                   | 0.007 seconds |
| mv pixmaps /tmp/aflores8-pixmaps    | 1.658 seconds |
| rm -r /tmp/aflores8-pixmaps         | 0.008 seconds |

1. The latter move takes longer as it has to both copies files to the new directory and remove the old directory and its files 
rather than just rename the directory like the first move.
2. With the remove command, it only deletes the files, the move command both copies the files and removes them from their old
location.

Activity 3

1. bc < math.txt
2. bc < math.txt &> result.txt
3. bc < math.txt > result.txt 2> /dev/null
4. cat math.txt | bc
It is less efficient as the redirection command only sends the redirected file to the input of the bc command. Using a cat command 
and a pipeline causes sends the output of the first command, the cat command, into the input of the bc command, requiring more 
commands than redirection.

Activity 4

1. cat /etc/passwd | grep /sbin/nologin | wc -l
2. who | cut -d" " -f1 | sort | uniq | wc -l
3. ls -l -S /etc | sort -k 5 -n -r | head -5 2> dev/null
4. aux |grep bash | wc -l

Activity 5

1.a. The ctrl-c command doesn't work. Stopping it with ctrl-z then kill it with kill %1 doesn't work.
b. The kill -KILL [PID] command terminates the process. The kill -9 [PID] command terminates it as well
2.a. kill -KILL $(ps -u aflores8 | grep TROLL)
b. killall -u aflores8 -KILL TROLL
3. The killall -u aflores8 -HUP TROLL command causes the TROLL program to produce song lyrics rather than taunts.







