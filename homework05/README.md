Homework 05
===========

Activity 1: Hulk Smash

1. The permutations were created using a recursive function that yields a character to add to the permutation to form strings of the
specified length. It creates and adds a character from the alphabet to the permuation, which starts blank. This is yielded and is 
taken in in the next recursion, where another character is added, this is repeated until each necessary string is created when it is
called by the smash function.
The permutations go through a list comprehension, where if their md5 hashes match any of the elements in the hashes, then the matching
permutation is added to list. After going through all the permutations, the list is returned.
To process on multiple cores, I used the prefix argument for smash to split up the work, where each prefix is given to a process, and
it uses that prefix in addition to the permutations to create the passwords that it compares the hashes to. The prefix list is mapped
using the pool to a partial function, which only takes in prefixes, where all the other arguments are already set.
The code was verified using the tests and by comparing the given hashes to hashes of strings of various lengths that have been translated 
to md5.

2.
Processes   Time
1           1:23:22.67
2           43:51.50
4           29:10.84
8           17:57.08
16          24:44.63

While using more processes usually leads to a faster result, if you try to use more processes than you have, then it might take longer
due to overhead as it tries to use more processes than it has.

3. A longer password is much difficult to brute force than a more complex one. A more complex password will be faster to get as it
takes less permutations to find the matching hash. At a certain point a long enough password would take a completely unreasonable 
amount of time to crack using brute-force as there would be too many permutations to create to try and find it. The number of total
permutations for a length n and an alphabet a is a^n, which is a much higher time complexity than that of a password of complex
characters.

Activity 2: Inverted Index
1. The map program keeps track of the line number by using an iterator i, that iterates each time a new line is read.
It removes undesirable characters by using a list comprehension that puts all the desired characters into a list while ignoring the 
undesirable characters, then it joins the list back together to be printed as the word.

2. The program creates a dictionary with a key which is the word and a key that is the line numbers that it appears on.


