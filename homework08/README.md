Homework 08
===========

Activity 1: Map Library

1. Briefly describe what memory you allocated in entry_create and what memory you deallocated in entry_delete.
How did you handle the recursive flag?
The entry_create function allocates the sizeof(Entry), then allocates a string for the key value. The function
then checks to see if the Entry is of type STRING. If so, then it allocates a string for the value of that
Entry. Otherwise, it sets it to a number value. In entry_delete it frees the value if it is a string, the key,
and then the Entry itself. To do it recursively, the function calls entry_delete recursively on the next Entry
until there are none left, deleting any strings and entries after all of them are called.

2. The map_create function allocates the sizeof(Map) and array of Entry objcts using calloc. When being
deallocated in map_delete, first the Entry objects are deallocated, then the Entry array is deallocated, and 
lastly the map itself is deallocated. To deallocate the inner Entry objects, the map_delete function goes
through each bucket and frees the bucket recursively.

3. In map_resize a new array of entries is allocated. From the old array, each entry is rehashed and placed within
the new array. When all of the entries are placed in the new array, the old array is deallocated. This function
is called when the load capacity is reached when inserting a new Entry into the map. The function grows the map
by taking in a new capacity that is twice the size as the old one to decrease the ratio of entries to capacity.

4. Briefly describe how your map_insert function works and analyze its time complexity and space complexity in
terms of Big-O notation (both average and worst case).
The map_insert function works by first checking the load capacity to see if the map needs to be resized. Then it
It then finds the bucket that the value belongs to using the hash function. It goes through the list of entries
in the bucket, if it finds a key that matches the key given, then it will update that entry to the new value. If
no other key is found then an entry is created at the end of the bucket's list and the size is incremented up by
one. The time complexity is O(1) on average amortized and in the worst case has a O(n) time complexity. The space
complexity for the function is O(1) as no recursion occurs.

5. The map_search function works by first finding the bucket that the key given belongs to using the hash function.
Then, using a pointer in that bucket's list, it iterates through comparing the strings of the entries to the given
key. If there is a match the function returns the Entry the pointer is pointing at, otherwise it returns NULL.
The time complexity is O(1)on average amortized and in the worst case has a O(n) time complexity. The space
complexity for the function is O(1) as no recursion occurs.

6. The map_remove function works by first using map_search to see if the key correlates to an Entry, if not it
returns false to show that it didn't successfully remove anything. If there is a match, a pointer iterates through
the bucket's list until the next Entry from the pointer is equal to the matched Entry. The Entry next from the
matched Entry is also saved. The matched Entry is finally deleted using entry_delete with no recursion and the
previous and next entries are linked together by setting the previous Entry's next pointer to Entry that was next
from the matched Entry. The time complexity is O(1)on average amortized and in the worst case has a O(n) time 
complexity. The space complexity for the function is O(1) as no recursion occurs.

Activity 2: Word Frequencies

1. Based on the benchmark, with more items all of the items increased in the time it took, with the tests having a
higher initial load capacity taking more time overall. In terms of space, as the number of items increased, the
space required increased a lot more in the tests with smaller initial load capacities.
These results aren't surprising as it makes sense that the smaller initial load capacities would require more space,
as they would have to increase size more than the others and may even end up with higher load capacities than those
with larger initial load capacities depending on how much they double in capacity.

2. Some advantages that a treap has is that treap only uses as much space as it needs, a hash table occasionally blows
up and needs to grow, increasing the time required while a treap is constant, and currently we don't no a way to shrink 
a hash table while you can change the size of a treap as you please. However, a hash table does better in terms of time
complexity as it has a set time complexity of O(logn). Additionally, they can be hard to implement correctly. 
If I had to use one in a map, I would prefer to use the hash table as being able to put them into buckets using an
integer index is easier to understand and implement for me. However, it all depends on what I'm using it for, as for 
certain applications one is better than the other.
