Grading - Homework 08
=====================

Activity 1
----------

- 0.25  Q4. Worst case space is O(n) (resize)

11.75 / 12

Activity 2
----------

- 0.25  Table doesn't go to 10,000,000
- 0.25  Treap doesn't overallocate memory

2.5 / 3

Total
-----

14.25 / 15
