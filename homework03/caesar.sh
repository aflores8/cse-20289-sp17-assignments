#!/bin/bash

#caesar.sh
#Name: Alan Flores
usage(){
  cat <<EOF
Usage: $(basename $0)

This program will read from stdin and rotate (shift right) the text by
the specified rotation.  If none is specified, then the default value is
13.
EOF
exit $1
}
UPPERSOURCE=ABCDEFGHIJKLMNOPQRSTUVWXYZ
LOWERSOURCE=abcdefghijklmnopqrstuvwxyz
if [ -z $1 ]; then
  SHIFT=13
  SHIFTS=14
elif [ $1 -gt 0 ]; then
  SHIFT=$(($1%26))
  SHIFTS=$(($SHIFT%26 + 1))
else
  usage 1
fi
TARGET1=`echo $UPPERSOURCE | cut -c -$SHIFT`
TARGET2=`echo $UPPERSOURCE | cut -c $SHIFTS-26`
TARGET1a=`echo $LOWERSOURCE | cut -c -$SHIFT` #lowercase part of the shift
TARGET2a=`echo $LOWERSOURCE | cut -c $SHIFTS-26` 
TARGET=$TARGET2$TARGET1$TARGET2a$TARGET1a

tr $UPPERSOURCE$LOWERSOURCE $TARGET