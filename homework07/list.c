/* list.c */

#include "list.h"

/* Internal Function Prototypes */

static struct node*	reverse(struct node *curr, struct node *next);
static struct node *	msort(struct node *head, node_compare f);
static void		split(struct node *head, struct node **left, struct node **right);
static struct node *	merge(struct node *left, struct node *right, node_compare f);

/**
 * Create list.
 *
 * This allocates a list that the user must later deallocate.
 * @return  Allocated list structure.
 */
struct list * list_create(){
  /* allocate new list*/
  struct list *newlist = malloc(sizeof(struct list));
  /* set fields */
  newlist->head = NULL;
  newlist->tail = NULL;
  newlist->size = 0;
  return newlist;
}

/**
 * Delete list.
 *
 * This deallocates the given list along with the nodes inside the list.
 * @param   l	    List to deallocate.
 * @return  NULL pointer.
 */
struct list * list_delete(struct list *l){
  if(l){
    node_delete(l->head, true);
    free(l);
  }
  return NULL;
}

/**
 * Push front.
 *
 * This adds a new node containing the given string to the front of the list.
 * @param   l	    List structure.
 * @param   s	    String.
 */
void list_push_front(struct list *l, char *s){
  struct node *newnode = node_create(s, l->head);
  l->head = newnode;
  l->size++;
  if(!l->tail){
    l->tail = newnode;
  }
}

/**
 * Push back.
 *
 * This adds a new node containing the given string to the back of the list.
 * @param   l	    List structure.
 * @param   s	    String.
 */
void list_push_back(struct list *l, char *s){
  struct node *newnode = node_create(s, NULL);
  if(!l->head){
    l->head = newnode;
  }
  if(l->tail){
    l->tail->next = newnode;
  }
  l->tail = newnode;
  l->size++;
}

/**
 * Dump list to stream.
 *
 * This dumps out all the nodes in the list to the given stream.
 * @param   l	    List structure.
 * @param   stream  File stream.
 */
void list_dump(struct list *l, FILE *stream){
  struct node * temp = l->head;
  while(temp){
    node_dump(temp, stream);
    temp = temp->next;
  }
}

/**
 * Convert list to array.
 *
 * This copies the pointers to nodes in the list to a newly allocate array that
 * the user must later deallocate.
 * @param   l	    List structure.
 * @return  Allocate array of pointers to nodes.
 */
struct node **list_to_array(struct list *l){
  struct node **arr = malloc(sizeof(struct node*)*l->size);
  struct node *temp = l->head;
  for(size_t i = 0; i < l->size; i++){
    arr[i] = temp;
    temp = temp->next;
  }
  return arr;
}

/**
 * Sort list using qsort.
 *
 * This sorts the list using the qsort function from the standard C library.
 * @param   l	    List structure.
 * @param   f	    Node comparison function.
 */
void list_qsort(struct list *l, node_compare f){
  struct node **arrList = list_to_array(l);
  /* Sort the new array */
  if(f == node_compare_string){
    qsort(arrList, l->size, sizeof(struct node*), f);
  }
  else if(f == node_compare_number){
    qsort(arrList, l->size, sizeof(struct node*), f);
  }
  /* Update pointers */
  l->head = arrList[0];
  for(size_t i = 0; i < l->size; i++){
    if(i < l->size-1){
      arrList[i]->next = arrList[i+1];
    }
    else{
      l->tail = arrList[i];
      arrList[i]->next = NULL;
    }
  }
  free(arrList);
}

/**
 * Reverse list.
 *
 * This reverses the list.
 * @param   l	    List structure.
 */
void list_reverse(struct list *l){
  l->tail = l->head;
  l->head = reverse(l->head, NULL);
}

/**
 * Reverse node.
 *
 * This internal function recursively reverses the node.
 * @param   curr    The current node.
 * @param   prev    The previous node.
 * @return  The new head of the singly-linked list.
 */
struct node* reverse(struct node *curr, struct node *prev){
  struct node *tail = curr;
  if(curr->next){
    tail = reverse(curr->next, curr);      
  }
  curr->next = prev;  
  return tail;
}

/**
 * Sort list using merge sort.
 *
 * This sorts the list using a custom implementation of merge sort.
 * @param   l	    List structure.
 * @param   f	    Node comparison function.
 */
void list_msort(struct list *l, node_compare f){
  l->head = msort(l->head, f);
  l->tail = l->head;
  while(l->tail->next){
    l->tail = l->tail->next;
  }
}

/**
 * Performs recursive merge sort.
 *
 * This internal function performs a recursive merge sort on the singly-linked
 * list starting with head.
 * @param   head    The first node in a singly-linked list.
 * @param   f	    Node comparison function.
 * @return  The new head of the list.
 */
struct node * msort(struct node *head, node_compare f){
  struct node *left = NULL;
  struct node *right = NULL;
  if(!head || !head->next){
    return head;
  }
  split(head, &left, &right); /* divide */
  left = msort(left, f); /* conquer */
  right = msort(right, f);
  head = merge(left, right, f);
  return head;
}

/**
 * Splits the list.
 *
 * This internal function splits the singly-linked list starting with head into
 * left and right sublists.
 * @param   head    The first node in a singly-linked list.
 * @param   left    The left sublist.
 * @param   right   The right sublist.
 */
void split(struct node *head, struct node **left, struct node **right){
  struct node *slow = NULL;
  struct node *fast = NULL;
  struct node *tail = NULL;
  if(!head){
    return;
  }
  else if(!head->next){
    *left = head;
    *right = NULL;
    tail = *left;
    tail->next = NULL;
    return;
  }
  else{
    slow = head;
    fast = head;
    tail = head;
    while(fast && fast->next){
      tail = slow;
      slow = slow->next;
      fast = fast->next->next;
    }
    *left = head;
    *right = slow;
    tail->next = NULL;
  }
}

/**
 * Merge sublists.
 *
 * This internal function merges the left and right sublists into one ordered
 * list.
 * @param   left    The left sublist.
 * @param   right   The right sublist.
 * @param   f	    Node comparison function.
 * @return  The new head of the list.
 */
struct node * merge(struct node *left, struct node *right, node_compare f){
  struct node *head = NULL;
  struct node *temp = NULL;
  struct node *tail = NULL;
  if(!left){
    return right;
  }
  else if(!right){
    return left;
  }
  if(f(&left, &right) < 0){
    head = left;
    left = left->next;
  }
  else{
    head = right;
    right = right->next;
  }
  tail = head;
  while(left && right){
    if(f(&left, &right) < 0){
      temp = left;
      left = left->next;
    }
    else{
      temp = right;
      right = right->next;
    }
    tail->next = temp;
    tail = temp;
  }
  if(left){
    tail->next = left;
  }
  else{
    tail->next = right;
  }
  return head;
}
