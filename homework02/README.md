Homework 02
===========

Activity 1
1. I used $1 to check for arguments, if there were arguments, then the program moved on to the case, but if there were no
arguments, then it would go to the else statement, which echoed the proper usage of the program.
2. Using a case statement, the program checks the end of each of the names put through as arguments, should they end in
the appropriate letters, then the case uses the command listed below on it.
3. The most difficult part of this script was figuring out which flag to the tar command I needed for each different type
of archive. It took a lot of reading and searching to find the appropriate flags needed to extract the archives correctly.

Activity 2
1. I displayed random messages to the user by using the shuf -e -n 1 command followed by the list of possible quotes, this
was then piped into the cowsay command. The shuf command picked 1 random member of that list, and then the cowsay commmand
displayed it.
2. I handled the signals using trap bye 1 2 15, which traps the signal and goes through the bye function, which sends the
user off with a goodbye message and then exits the program.
3. The program reads user input through a while loop, which keeps trying to read the user's input until the value of that
input has changed from nothing, otherwise it keeps prompting.
4. I feel the most challlenging part was to figure out how to get the shuf command to work, but after asking some people
how it worked, it became a lot clearer and I was able to use it multiple times in the code without problem.

Activity 3
1. My first step was to scan 'xavier.h4x0r.space' for an HTTP port:
$ nc -z xavier.h4x0r.space 9000-9999
Connection to xavier.h4x0r.space 9097 port [tcp/*] succeeded!
Connection to xavier.h4x0r.space 9111 port [tcp/*] succeeded!
Connection to xavier.h4x0r.space 9876 port [tcp/sd] succeeded!
As can be seen, there are 3 ports in the '9000' - '9999'

3. There wasn't much I thought I could use, so I tried the 9111 port:
nc xavier.h4x0r.space 9111
 ________________________
< Hello, who may you be? >
 ------------------------
 \     /\  ___  /\
  \   // \/   \/ \\
     ((    O O    ))
      \\ /     \ //
       \/  | |  \/
        |  | |  |
        |  | |  |
        |   o   |
        | |   | |
        |m|   |m|
NAME? Alan

It prompted me for a name, so I put my name in.
 ___________________________________
/ Hmm... Alan?                      \
|                                   |
| That name sounds familiar... what |
\ message do you have for me?       /
 -----------------------------------
 \     /\  ___  /\
  \   // \/   \/ \\
     ((    O O    ))
      \\ /     \ //
       \/  | |  \/
        |  | |  |
        |  | |  |
        |   o   |
        | |   | |
        |m|   |m|
MESSAGE? I don't know

However, I had no clue what to put as a message, so I just tried "I don't know" 
 _________________________________________
/ I'm sorry sugar, but I don't understand \
| this message:                           |
|                                         |
| I don't know                            |
|                                         |
| Perhaps, it wasn't meant for you.       |
|                                         |
| Why don't you go back to the DOORMAN or |
\ BOBBIT?                                 /
 -----------------------------------------
 \     /\  ___  /\
  \   // \/   \/ \\
     ((    O O    ))
      \\ /     \ //
       \/  | |  \/
        |  | |  |
        |  | |  |
        |   o   |
        | |   | |
        |m|   |m|
The program ended with that, and went back to the normal terminal.

2. Next I tried to access the HTTP server port 9097 using curl as nc didn't give me anything from 9097:

curl xavier.h4x0r.space 9097
returns an index

4. I decided that I would start using the curl command on the port to see if that worked:
curl xavier.h4x0r.space:9876
 ________________________________________
/ Halt! Who goes there?                  \
|                                        |
| If you seek the ORACLE, you must come  |
| back and _request_ the DOORMAN at      |
| /{NETID}/{PASSCODE}!                   |
|                                        |
| To retrieve your PASSCODE you must     |
| first _find_ your LOCKBOX which is     |
| located somewhere in                   |
| ~pbui/pub/oracle/lockboxes.            |
|                                        |
| Once the LOCKBOX has been located, you |
| must use your hacking skills to        |
| _bruteforce_ the LOCKBOX program until |
| it reveals the passcode!               |
|                                        |
\ Good luck!                             /
 ---------------------------------------- 
                       \                    ^    /^
                        \                  / \  // \
                         \   |\___/|      /   \//  .\
                          \  /O  O  \__  /    //  | \ \           *----*
                            /     /  \/_/    //   |  \  \          \   |
                            @___@`    \/_   //    |   \   \         \/\ \
                           0/0/|       \/_ //     |    \    \         \  \
                       0/0/0/0/|        \///      |     \     \       |  |
                    0/0/0/0/0/_|_ /   (  //       |      \     _\     |  /
                 0/0/0/0/0/0/`/,_ _ _/  ) ; -.    |    _ _\.-~       /   /
                             ,-}        _      *-.|.-~-.           .~    ~
            \     \__/        `/\      /                 ~-. _ .-~      /
             \____(oo)           *.   }            {                   /
             (    (--)          .----~-.\        \-`                 .~
             //__\\  \__ Ack!   ///.----..<        \             _ -~
            //    \\               ///-._ _ _ _ _ _ _{^ - - - - ~

5. So I went to try and find the LOCKBOX. First I went to the location given:
cd ~pbui/pub/oracle/lockboxes
./95abb267/1ea93a31/51979f59/5f3ac66e/aflores8.lockbox

6. I used find to get to find where my lockbox was located:
find . -name  aflores8.lockbox

7. I then went to the directory it was in:
cd 95abb267/1ea93a31/51979f59/5f3ac66e/

8. I then used a file called brute_force.sh with loop and strings to brute force the lockbox:

#!/bin/sh

lockbox=/afs/nd.edu/user15/pbui/pub/oracle/lockboxes/95abb267/1ea93a31/51979f59/5f3ac66e/aflores8.lockbox
for string in $(strings $lockbox); do
 $lockbox $string
done

./brute_force.sh
542ad4d871b6e30720a2787a6e453ebb

9. I then used curl on port 9876:
curl xavier.h4x0r.space:9876/aflores8/542ad4d871b6e30720a2787a6e453ebb
 _________________________________________
/ Ah yes, aflores8... I've been waiting   \
| for you.                                |
|                                         |
| The ORACLE looks forward to talking to  |
| you, but you must authenticate yourself |
| with our agent, BOBBIT, who will give   |
| you a message for the ORACLE.           |
|                                         |
| He can be found hidden in plain sight   |
| on Slack. Simply send him a direct      |
| message in the form "!verify netid      |
| passcode". Be sure to use your NETID    |
| and the PASSCODE you retrieved from the |
| LOCKBOX.                                |
|                                         |
| Once you have the message from BOBBIT,  |
| proceed to port 9111 and deliver the    |
| message to the ORACLE.                  |
|                                         |
| Hurry! The ORACLE is wise, but she is   |
\ not patient!                            /
 -----------------------------------------
   \
    \              ....
           ........    .
          .            .
         .             .
.........              .......
..............................

Elephant inside ASCII snake

10.I went to slack to get the message:
!verify aflores8 542ad4d871b6e30720a2787a6e453ebb

Bobbit [4:32 PM] 
@aflores: Hey aflores8! Please tell the ORACLE the following MESSAGE: bnN5YmVyZjg9MTQ4NjE1NzU1OQ==

11. Then I went to the oracle using nc:
nc xavier.h4x0r.space 9111
 ________________________
< Hello, who may you be? >
 ------------------------
     \
      \
          oO)-.                       .-(Oo
         /__  _\                     /_  __\
         \  \(  |     ()~()         |  )/  /
          \__|\ |    (-___-)        | /|__/
          '  '--'    ==`-'==        '--'  '
NAME? aflores8
 ___________________________________
/ Hmm... aflores8?                  \
|                                   |
| That name sounds familiar... what |
\ message do you have for me?       /
 -----------------------------------
     \
      \
          oO)-.                       .-(Oo
         /__  _\                     /_  __\
         \  \(  |     ()~()         |  )/  /
          \__|\ |    (-___-)        | /|__/
          '  '--'    ==`-'==        '--'  '
MESSAGE? bnN5YmVyZjg9MTQ4NjE1NzU1OQ==
 _______________________________________
/ Ah yes... aflores8!                   \
|                                       |
| You're smarter than I thought. I can  |
| see why the instructor likes you.     |
|                                       |
| You met BOBBIT about 5 minutes ago... |
\ What took you so long?                /
 ---------------------------------------
     \
      \
          oO)-.                       .-(Oo
         /__  _\                     /_  __\
         \  \(  |     ()~()         |  )/  /
          \__|\ |    (-___-)        | /|__/
          '  '--'    ==`-'==        '--'  '
REASON? I forgot to log this the first time...

I hope you learned something from the ORACLE :]

Then the oracle activated the matrix and I used ctrl-c to exit.
