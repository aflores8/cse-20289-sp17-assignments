#!/usr/bin/env python2.7

import os
import re

# cat /etc/passwd
with open('/etc/passwd', 'r') as f:
    text = f.readlines()

# cut -d : -f 5 
fields = [line.split(':')[4] for line in text]
    
# grep '[uU]ser' | tr 'A-Z' 'a-z'
regex = '[uU]ser'
result = [x.lower() for x in fields if re.search(regex,x)]
# sort
for x in sorted(result):
    print x
