#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[]){
  DIR* directory;
  struct dirent* dir;
  char* filename;
  int success;
  struct stat stats;
  directory = opendir(".");
  if(directory == NULL){
    perror("Could not find directory");
  }
  dir = readdir(directory);
  if(dir == NULL){
    perror("Empty directory");
  }
  while(dir != NULL){
    if(strcmp(dir->d_name, ".") == 0 || strcmp(dir->d_name, "..") == 0){
    }
    else{
      filename = dir->d_name;
      stat(filename, &stats);
      printf("%s %zd\n", filename, stats.st_size);
    }
    dir = readdir(directory);
  }
  success = closedir(directory);
  return success;
}