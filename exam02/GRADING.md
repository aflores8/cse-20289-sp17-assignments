GRADING - Exam 02
=================

- Identification:   2.25
- Web Scraping:     3.75
- Generators:       5
- Concurrency:      6
- Translation:      5.75
- Total:	    22.75

Comments
--------

- 0.25  Translation: Scripts did not work due to DOS line endings
