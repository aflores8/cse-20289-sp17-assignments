/* translate.c: string translator */

#include "stringutils.h"

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Globals */

char *PROGRAM_NAME = NULL;
enum{ 
  STRIP = 1<<1,
  REVERSE = 1<<2,
  REVERSE_W = 1<<3,
  LOWER = 1<<4,
  UPPER = 1<<5
};

/* Functions */

void usage(int status) {
  fprintf(stderr, "Usage: %s SOURCE TARGET\n\n", PROGRAM_NAME);
  fprintf(stderr, "Post Translation filters:\n\n");
  fprintf(stderr, "   -s     Strip whitespace\n");
  fprintf(stderr, "   -r     Reverse line\n");
  fprintf(stderr, "   -w     Reverse words in line\n");
  fprintf(stderr, "   -l     Convert to lowercase\n");
  fprintf(stderr, "   -u     Convert to uppercase\n");
  exit(status);
}

void translate_stream(FILE *stream, char *source, char *target, int mode){
  char buffer[BUFSIZ];
  char *c = buffer;
  while(fgets(c, BUFSIZ, stream)){
    c = string_chomp(c);
    if(*source && *target){
      c = string_translate(c, source, target);
    }
    if(mode & STRIP){
      c = string_strip(c);
    }
    if(mode & REVERSE){
      c = string_reverse(c);
    }
    if(mode & REVERSE_W){
      c = string_reverse_words(c);
    }
    if(mode & LOWER){
      c = string_lowercase(c);
    }
    if(mode & UPPER){
      c = string_uppercase(c);
    }
    printf("%s\n", c);
  }
}

/* Main Execution */
int main(int argc, char *argv[]){
  /* Parse command line arguments */
  int argind = 1;
  int mode = 0;
  char* SOURCE = "";
  char* TARGET = "";
  PROGRAM_NAME = argv[0];
  while(argind < argc && strlen(argv[argind]) > 1 && argv[argind][0] == '-'){
    char *arg = argv[argind++];
    switch(arg[1]){
      case 's':
        mode |= STRIP;
        break;
      case 'r':
        mode |= REVERSE;
        break;
      case 'w':
        mode |= REVERSE_W;
        break;
      case 'l':
        mode |= LOWER;
        break;
      case 'u':
        mode |= UPPER;
        break;
      case 'h':
        usage(0);
        break;
      default:
        usage(1);
        break;
    }
  }
  if(argind <= argc - 2){
    SOURCE = argv[argind];
    TARGET = argv[argind+1];
  }
  /* Translate Stream */
  translate_stream(stdin, SOURCE, TARGET, mode);
  return EXIT_SUCCESS;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
