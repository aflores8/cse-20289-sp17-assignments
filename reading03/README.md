Reading 03
==========

1. echo "All your base are belong to us" | tr a-z A-Z
The tr a-z A-Z command replaces all lowercase letters to uppercase.
2. echo "monkeys love bananas" | sed 's/monkeys/gorillaz/'
sed 's/monkeys/gorillaz/' replaces one instance of monkeys with gorillaz. If you wanted to change multiple you would put
g before the apostrophe.
3. echo "     monkeys love bananas" | sed 's/[[:space:]]*//'
sed 's/[[:space:]]*//' removes all leading whitespace from the string.
4. cat /etc/passwd | grep 0:0 | awk -F "root:/root:" '{print $2}'
grep finds the root, while the awk command prints what's after the root file name.
5. cat /etc/passwd | sed 's_/bin/bash_/usr/bin/python_'| sed 's_/bin/csh_/usr/bin/python_' | sed 's_/bin/tcsh_/usr/bin/python_' | grep python
The underscore is required when you use / in sed, allowing all three to be changed to /usr/bin/python
6. cat /etc/passwd | grep '^(4).*(7)$'
7. comm [-1] [-2] file1 file2
comm [-1] and [-2] suppress the lines that are unique for the first and second files respectively
8. comm [-3] file1 file2
comm [-3] suppresses the lines that the files have in commmon